import { CellsPage } from '@cells/cells-page';
import { html } from 'lit-element';
import stylesPage from '../../elements/styles/main-styles';
import stylesButtons from '../../elements/styles/buttons-styles';
import '@cells-components/cells-template-paper-drawer-panel';
import '@bbva-web-components/bbva-button-default/bbva-button-default';
import '../../elements/components/number-fcr-component/number-fcr-component';
import '../../elements/components/onr-component/onr-component';
import '../../elements/components/list-operation-component/list-operation-component';
import '../../elements/dm/smart-assistant-dm/smart-assistant-dm';

/* eslint-disable new-cap */
class InitPage extends CellsPage {
  static get is() {
    return 'init-page';
  }

  static get properties() {
    return {
      fcrs: Array,
      fcrType: String,
      body: Object,
      enableRegister: Boolean
    };
  }

  constructor() {
    super();
    this.fcrType = '';
    this.body = {};
    this.enableRegister = false;
    this.fcrs = [
      {
        text: 'Cancelacion de cuentas',
        value: 'ACCOUNT_CANCEL',
      },
      {
        text: 'Cancelacion de Seguros',
        value: 'INSURANCE_CANCEL',
      },
      {
        text: 'Devolucion de membresias',
        value: 'MEMBERSHIP_RETURN',
      },
      {
        text: 'Operaciones no reconocidas',
        value: 'ONR'
      }
    ];
  }

  get dm() {
    return this.shadowRoot.querySelector('smart-assistant-dm');
  }

  get componentFcrTpl() {
    if (this.fcrType === 'ONR') {
      return html`<onr-component></onr-component>`;
    } if (this.fcrType === 'ACCOUNT_CANCEL') {
      return html`<number-fcr-component label="Número de cuenta"></number-fcr-component>`;
    } if (this.fcrType === 'MEMBERSHIP_RETURN') {
      return html`<number-fcr-component label="Número de tarjeta"></number-fcr-component>`;
    } if (this.fcrType === 'INSURANCE_CANCEL') {
      return html`<number-fcr-component label="Número de contrato"></number-fcr-component>`;
    } else {
      return html`<div class="not-selections-fcr">
        Seleccione el tipo de fcr
      </div>`;
    }
  }

  selectedFcr({ detail }) {
    console.log('selectedFcr', detail);
    this.fcrType = detail.value;
    this.enableRegister = true;
    this.shadowRoot.querySelector('#btnRegistro').removeAttribute('disabled');
    this.shadowRoot.querySelector('#btnRegistro').classList.remove('disabled');
    this.requestUpdate();
  }

  registrar() {
    this.body = {
      customerId: '1234123275'
    };
    if (this.fcrType === 'ACCOUNT_CANCEL') {
      this.body = {
        accountNumber: this.shadowRoot.querySelector('number-fcr-component').getAccountNumber(),
        fcrType: 'ACCOUNT_CANCEL'
      };
    } if (this.fcrType === 'INSURANCE_CANCEL') {
      this.body = {
        contractNumber: this.shadowRoot.querySelector('number-fcr-component').getAccountNumber(),
        fcrType: 'INSURANCE_CANCEL'
      };
    } if (this.fcrType === 'MEMBERSHIP_RETURN ') {
      this.body = {
        cardNumber: this.shadowRoot.querySelector('number-fcr-component').getAccountNumber(),
        fcrType: 'MEMBERSHIP_RETURN '
      };
    } if (this.fcrType === 'ONR ') {
      this.body = {
        cardNumber: this.shadowRoot.querySelector('number-fcr-component').getAccountNumber(),
        fcrType: 'ONR '
      };
    }
    console.log('Registrar', this.body);
  }

  render() {
    return html` <cells-template-paper-drawer-panel mode="seamed">
      <div slot="app__main" class="container-page">
        <main class="main-page vertical">
          <section class="section-content form">
            <cells-card-panel headerTitle="Registro de FCR'S" class="panels">
              <div slot="body">
                <cells-combobox
                  .items="${this.fcrs}"
                  label="Tipo de FCR"
                  @option-selected="${this.selectedFcr}"
                ></cells-combobox>

                ${this.componentFcrTpl}

                <div class="btn-contents">
                  <button id="btnRegistro"  @click="${this.registrar}"
                    class="button button-primary lg full-width disabled" disabled>
                  Registrar FCR
                  </button>
                </div>
              </div>
            </cells-card-panel>
          </section>
          <section class="section-content right-panel">
            <cells-card-panel headerTitle="Operaciones realizadas por FCR's" class="panels panel-listado">
              <div slot="body">
                <list-operation-component></list-operation-component>
              </div>
            </cells-card-panel>
          </section>
        </main>
        <smart-assistant-dm></smart-assistant-dm>
      </div>
    </cells-template-paper-drawer-panel>`;
  }

  onPageEnter() {
    this.loadOperations();
  }

  async loadOperations() {
    let response = await this.dm.listOperations();
    this.shadowRoot.querySelector('list-operation-component').operations = response.data.listOperation;
    console.log('loadOperations', response);
  }

  onPageLeave() {}

  static get styles() {
    return [stylesPage, stylesButtons];
  }
}

window.customElements.define(InitPage.is, InitPage);
