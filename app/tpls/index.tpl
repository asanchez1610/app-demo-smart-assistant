<!doctype html>

<html lang="es">

<head>
  <meta charset="utf-8">
  <meta name="description" content="">
  <meta name="format-detection" content="telephone=no">
  <meta name="msapplication-tap-highlight" content="no">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Motor Smart Assistant</title>
  <!-- Place favicon.ico in the `app/` directory -->

  <!-- Chrome for Android theme color -->
  <meta name="theme-color" content="#303F9F">

  <!-- Web Application Manifest -->
  <link rel="manifest" href="manifest.json">

  <!-- Tile color for Win8 -->
  <meta name="msapplication-TileColor" content="#3372DF">

  <!-- Add to homescreen for Chrome on Android -->
  <meta name="mobile-web-app-capable" content="yes">
  <meta name="application-name" content="staticScaffold">
  <link rel="icon" sizes="192x192" href="resources/images/touch/chrome-touch-icon-192x192.png">

  <!-- Add to homescreen for Safari on iOS -->
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <meta name="apple-mobile-web-app-title" content="staticScaffold">
  <link rel="apple-touch-icon" href="resources/images/touch/apple-touch-icon.png">

  <!-- Tile icon for Win8 (144x144) -->
  <meta name="msapplication-TileImage" content="resources/images/touch/ms-touch-icon-144x144-precomposed.png">

  <link rel="stylesheet" href="styles/main.css">
  <script>
    window.IntlMsg = window.IntlMsg || {};
    window.IntlMsg.lang = 'es-ES';
  </script>
</head>

<body class="fullbleed layout vertical loading">
  <div id="splash"></div>
  <div id="app__content"></div>

  <!-- for a11y purposes -->
  <div id="announcer" aria-live="polite"></div>

  <cells-header-menu-app
            user='{"id":"5fc9400b3540d27ff226e289","email":"asanchez.sys@gmail.com","registro":"P027140","nombres":"Jose Arturo","apellidos":"Sanchez Soto","oficina":{"name":"Miraflores","code":"0333"},"sexo":"M","urlImgProfile":"https://cdn.iconscout.com/icon/free/png-256/avatar-370-456322.png","rol":{"name":"Administrador","code":"admin"},"token":""}'
            imgFProfile="resources/images/avatar-famele.svg"
            imgMProfile="resources/images/avatar-male.svg"
            titleApp="Smart Assitant"
            logo="resources/images/logo_bbva_blanco.svg"
            logoMenu="resources/images/logo_bbva_blanco.svg"
            viewMode="vertical"
          ></cells-header-menu-app>
          <cells-mask-loading-demo ></cells-mask-loading-demo>
  <script>
     window.onload = async() => {
        const options = await fetch('resources/mock/options.json').then(response => response.json());
        console.log(document.querySelector('cells-header-menu-app'))
        document.querySelector('cells-header-menu-app').items = options;
      };

      window.addEventListener('before-request', () => {
        document.querySelector('cells-mask-loading-demo').show = true; 
      });

      window.addEventListener('complete-request', () => {
        document.querySelector('cells-mask-loading-demo').show = false; 
      });

  </script>
  <script src="vendor/bowser.min.js"></script>
  <script type="module" src="scripts/app-module.js"></script>

</body>

</html>
