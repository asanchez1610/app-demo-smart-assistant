import { BaseElement } from '../../commons/BaseElement';
import { SmartAssistantService } from './SmartAssistantService';

class SmartAssistantDm extends BaseElement {

  constructor() {
    super();
  }

  get service() {
      return new SmartAssistantService();
  }

  async listOperations() {
    this.dispatch('before-request');
    const { response } = await this.service.findOperations();
    this.dispatch('complete-request', response);
    return response;
  }

}
customElements.define('smart-assistant-dm', SmartAssistantDm);
