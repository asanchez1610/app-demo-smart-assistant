import { BaseService } from '../../commons/BaseService';

export class SmartAssistantService extends BaseService {
    constructor() {
      super();
    }

    async findOperations() {
        const request = await this.generateRequest({
          path: `${this.paths.listOperation}`,
          method: 'POST',
          body: {
            'customerId' : '1234123275'
          }
        });
        const { status } = request;
        let response;
        if(status === 204) {
          response = {};
        } else {
          response = await request.json();
        }
        return { response, status };
      }
    

}  