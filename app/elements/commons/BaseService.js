export class BaseService {
  constructor() {
    this.host = window.AppConfig.services.host;
    this.paths = window.AppConfig.services.paths;
  }

  async generateRequest({
    method = 'GET',
    path,
    body,
    headers,
    host
  }) {
    let sendHeaders = new Headers();
    sendHeaders.append('Content-Type', 'application/json');
    if (headers) {
      headers.forEach((header) => {
        sendHeaders.append(header.name, header.value);
      });
    }
    const requestOptions = {
      method: method,
      headers: sendHeaders
    };
    if (body) {
      if (typeof body === 'object') {
        requestOptions.body = JSON.stringify(body);
      } else {
        requestOptions.body = body;
      }
    }
    return await fetch(`${host ? host : this.host}/${path}`, requestOptions);
  }
}
