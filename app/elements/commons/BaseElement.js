import { LitElement, html } from 'lit-element';

export class BaseElement extends LitElement {
  constructor() {
    super();
  }

  extract(data, keys, value) {
    let ret;
    if (!this.isEmpty(data) && !this.isEmpty(keys)) {
      let split = keys.split('.');
      ret = data[split.shift()];
      while (ret && split.length) {
        ret = ret[split.shift()];
      }
    }
    return this.isEmpty(ret) && value !== null ? value : ret;
  }

  isEmpty(evaluate) {
    switch (typeof evaluate) {
      case 'object':
        return evaluate === null || Object.keys(evaluate).length === 0;
      case 'string':
        return evaluate === '';
      case 'undefined':
        return true;
      default:
        return false;
    }
  }

  isNotEmpty(evaluate) {
    return !this.isEmpty(evaluate);
  }

  dispatch(name, detail) {
    const val = typeof detail === 'undefined' ? null : detail;
    this.dispatchEvent(
      new CustomEvent(name, {
        composed: true,
        bubbles: true,
        detail: val,
      })
    );
  }

  adaptStringHtml(stringHtml) {
    var t = document.createElement('template');
    t.innerHTML = stringHtml;
    return t.content.cloneNode(true);
  }

  applyObjectParamsInUrl(path, params) {
    let urlParams = Object.keys(params)
      .map(function(k) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
      })
      .join('&');
    return `${path}?${urlParams}`;
  }

  element(selector) {
    return this.shadowRoot.querySelector(selector);
  }

  elementsAll(selector) {
    return this.shadowRoot.querySelectorAll(selector);
  }

  getJsonFromQueryParams(path) {
    let query = path;
    let result = {};
    query.split('&').forEach(function(part) {
      let item = part.split('=');
      result[item[0]] = decodeURIComponent(item[1]);
    });
    return result;
  }
}
