import { css } from 'lit-element';
export default css`

.container-page {
    background-color: #E9E9E9;
    padding-top: 110px;
    padding: 20px;
    min-height: calc(100vh - 150px);
}

.container-page .vertical{
    margin-left: 60px;
    padding-top: 60px;
    min-height: calc(100vh - 100px);
}

.main-page {
    display: flex;
}

.section-content {
    min-height: calc(100vh - 100px)
}

.form{
    width: 35%;
}

.right-panel{
    width: 65%;
    margin-left: 20px;
}

.panels{
    background-color: #fff;
    --card-min-height: calc(100vh - 155px);
}

.panel-listado {
    --body-padding: 0px;
}

.not-selections-fcr {
    width: calc(100% - 20px);
    text-align: center;
    padding: 15px 10px 0px 10px;
}

.btn-contents {
    width: 100%;
    display: flex;
    align-items: center;
    justify-content: center;
    margin-top: 10px;
    flex: 1;
}



`;